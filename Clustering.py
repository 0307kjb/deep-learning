# 군집 알고리즘 적용 #
import cv2
import matplotlib.pyplot as plt
import numpy as np
import glob
if __name__ == '__main__':

    # 이미지 파일들을 갖고오는 경로
    path_dir_mask =".\\data\\mask\\*.jpg"
    path_dir_no_mask = ".\\data\\nomask\\*.jpg"

    # 각 이미지를 그레이스케일로 변경해서 가져옵니다.
    # 여기서 'dstack'은 (2,2) + (2, 2)데이터를 => (2,2,2) 이런식으로 쌓습니다.
    mask_data = glob.glob(path_dir_mask)
    data = np.array(cv2.resize(cv2.imread(mask_data[0], cv2.IMREAD_GRAYSCALE), dsize=(120, 120)))

    # 마스크 데이터
    for mask in mask_data:
        tmp = np.array(cv2.resize(cv2.imread(mask, cv2.IMREAD_GRAYSCALE), dsize=(120, 120)))
        data = np.dstack((data, tmp))

    mask_data = data.copy()

    # 노 마스크 데이터
    no_mask_data = glob.glob(path_dir_no_mask)

    data = np.array(cv2.resize(cv2.imread(no_mask_data[0], cv2.IMREAD_GRAYSCALE), dsize=(120, 120)))

    for no_mask in no_mask_data:
        tmp = np.array(cv2.resize(cv2.imread(no_mask, cv2.IMREAD_GRAYSCALE), dsize=(120, 120)))
        data = np.dstack((data, tmp))

    no_mask_data = data.copy()

    # 이미지 데이터를 1차원배열로 나타내는 과정
    tmp_mask = mask_data.reshape(120 * 120, -1)
    tmp_no_mask = no_mask_data.reshape(120 * 120, -1)

    # 1차원 배열로 데이터를 만들어 픽셀값 평균 계산
    fig, axs = plt.subplots(1, 2, figsize=(20, 5))
    axs[0].bar(range(120 * 120), np.mean(tmp_mask, axis=1))
    axs[1].bar(range(120 * 120), np.mean(tmp_no_mask, axis=1))
    plt.show()

    # 픽셀 평균 값 대표 이미지 출력
    # mask_mean = np.mean(tmp_mask, axis=1).reshape(120,120)
    # no_mask_mean = np.mean(tmp_no_mask, axis=1).reshape(120,120)
    # fig, axs = plt.subplots(1, 2, figsize=(20, 5))
    # axs[0].imshow(mask_mean, cmap='gray_r')
    # axs[1].imshow(no_mask_mean, cmap='gray_r')
    # plt.show()

    # 총 이미지에서 마스크 쓴 데이터와 가장 가까운 300개 샘플 추출
    total = np.dstack((mask_data, no_mask_data))

    mask_mean = np.mean(tmp_mask, axis=1).reshape(120, 120, 1)

    abs_diff = np.abs(total - mask_mean)
    abs_mean = np.mean(abs_diff, axis=(0, 1))

    # 평균값과 가까운 사진 고르기.
    fig, axs = plt.subplots(30, 10, figsize=(10, 10))
    index = np.argsort(abs_mean)[:300]
    for i in range(30):
        for j in range(10):
            axs[i, j].imshow(total[:, :, i*10+j], cmap='gray_r')
            axs[i,j].axis('off')

    plt.show()
