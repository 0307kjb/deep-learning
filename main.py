import cvlib as cv
import cv2

if __name__ == '__main__':

    webcam = cv2.VideoCapture(0)

    if not webcam.isOpened():
        print("open webcam")

    sample_num = 0
    captured_num = 0

    while webcam.isOpened():

        # read frame from webcam
        status, frame = webcam.read()

        if not status:
            break

        faces, confidences = cv.detect_face(frame)

        for idx, f in enumerate(faces):
            (startX, startY) = f[0], f[1]
            (endX, endY) = f[2], f[3]

            # draw rectangle over face
            cv2.rectangle(frame, (startX, startY), (endX, endY), (0, 255, 0), 2)

            if sample_num % 8 == 0:
                captured_num = captured_num + 1
                face_in_img = frame[startY:endY, startX:endX, :]
                cv2.imwrite('./data/mask' + str(captured_num) + '.jpg', face_in_img)  # 데이터 수집


        cv2.imshow('test', frame)

        k = cv2.waitKey(1)
        if k == 27: break

